import React, { Component } from "react";
import { Container, Text, View } from "native-base";
import { CheckBox } from "react-native";
import { VictoryLine, VictoryChart, VictoryTheme } from "victory-native";
import { deviceWidth } from "../../../native-base-theme/variables/commonColor.js";

interface IMainState {
  arrayLost?: Array<number>;
  arrayWin?: Array<number>;
  errorCount?: number;
  gamesLost?: number;
  gameState?: Array<{ letter: string, reward: number }>;
  gamesWin?: number;
  percentage?: number;
  rightShoot?: number;
  running?: boolean;
  selectedLetter?: string;
  selectedWord?: string;
  selectedWordLength?: number;
  showingGraph?: boolean;
  showingLosts?: boolean;
  showingRewards?: boolean;
  stateAlphabet?: Array<string>;
  wordArray?: Array<string>;
  wrongWordArray?: Array<string>;
}

interface IProps {
  updateMainState?: any;
  mainState?: IMainState;
}

interface IState { }

class Graphs extends Component<IProps, IState> {
  constructor(props) {
    super(props);
    this.state = {}
  }

  render() {
    const { mainState } = this.props;
    const { arrayWin, arrayLost } = mainState;

    const winData = arrayWin.map((actWin, i) => ({ x: i, y: actWin }));
    const lostData = arrayLost.map((actLost, i) => ({ x: i, y: actLost }));

    return (
      <Container>
        <View>
          <View>
            <CheckBox onValueChange={(value) => this.setState({ showingLosts: value })} value={false} />
            <Text style={{ marginTop: 5 }}>Derrotas</Text>
          </View>
          <VictoryChart width={deviceWidth - 60} theme={VictoryTheme.material}>
            {false &&
              <VictoryLine
                style={{
                  data: { stroke: "#ff0000", strokeWidth: 4 }
                }}
                data={lostData}
                x="x"
                y="y"
              />
            }
            <VictoryLine
              style={{
                data: { stroke: "#0000ff", strokeWidth: 4 }
              }}
              data={winData}
              x="x"
              y="y"
            />
          </VictoryChart>
        </View>
      </Container>
    )
  }
}

export default Graphs