import { StyleSheet } from "react-native";

export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        padding: 20
    },
    letterSpace: {
        borderBottomColor: "#000",
        borderBottomWidth: 2,
        marginRight: 5
    },
    wordContainer: {
        display: "flex",
        flexDirection: "row",
        alignItems: "flex-end",
        marginLeft: "-15%"
    },
    letter: {
        fontSize: 40,
        fontWeight: "bold",
        textAlign: "center"
    },
    gameContainer: {
        display: "flex",
        flexDirection: "row",
    },
    stick: {
        position: "absolute",
        left: 80,
        top: 29,
        height: 180,
        width: 80
    },
    head: {
        backgroundColor: "#fff",
        position: "absolute",
        left: 90,
        top: 29,
        height: 50,
        width: 60
    },
    body: {
        backgroundColor: "#fff",
        position: "absolute",
        left: 109,
        top: 75,
        height: 63,
        width: 21
    },
    leftArm: {
        backgroundColor: "#fff",
        position: "absolute",
        left: 80,
        top: 77,
        height: 60,
        width: 30
    },
    rightArm: {
        backgroundColor: "#fff",
        position: "absolute",
        left: 129,
        top: 77,
        height: 60,
        width: 32
    },
    leftLeg: {
        backgroundColor: "#fff",
        position: "absolute",
        left: 80,
        top: 136,
        height: 70,
        width: 39
    },
    rightLeg: {
        backgroundColor: "#fff",
        position: "absolute",
        left: 119,
        top: 136,
        height: 70,
        width: 39
    },
    wrongLettersContainer: {
        display: "flex",
        flexDirection: "row",
        flexWrap: "wrap",
        position: "absolute",
        top: 30,
        left: "25%",
        width: "75%"
    },
    wrongLetters: {
        color: "red",
        fontSize: 40,
        marginRight: 5
    },
    buttonStyle: { 
        backgroundColor: "#0000ff", 
        borderRadius: 5, 
        padding: 10, 
        position: "absolute", 
        top: 20, 
        left: 500, 
        zIndex: 9999 
    },
    alphabetContainer: {
        display: "flex",
        width: "100%",
        flexDirection: "row",
        justifyContent: "space-between",
        marginTop: 20
    },
    alphabetLetter: {
        borderRadius: 5,
        backgroundColor: "#00ff30",
        color: "#fff",
        paddingHorizontal: 8
    }
});