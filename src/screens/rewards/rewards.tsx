import React, { Component } from "react";
import { Container } from "native-base";
import { VictoryLine, VictoryChart, VictoryTheme } from "victory-native";
import { deviceWidth } from "../../../native-base-theme/variables/commonColor.js";
import { View, Text, Image, TouchableHighlight, Dimensions, CheckBox, Slider } from "react-native";

import rewardStyles from "./rewardStyles";
import alphabet from "../../helpers/alphabet";

interface IMainState {
  arrayLost?: Array<number>;
  arrayWin?: Array<number>;
  errorCount?: number;
  gamesLost?: number;
  gameState?: Array<{ letter: string, reward: number }>;
  gamesWin?: number;
  percentage?: number;
  rightShoot?: number;
  running?: boolean;
  selectedLetter?: string;
  selectedWord?: string;
  selectedWordLength?: number;
  showingGraph?: boolean;
  showingLosts?: boolean;
  showingRewards?: boolean;
  stateAlphabet?: Array<string>;
  wordArray?: Array<string>;
  wrongWordArray?: Array<string>;
}

interface IProps {
  updateMainState?: any;
  mainState?: IMainState;
}

interface IState { }

class Rewards extends Component<IProps, IState> {
  constructor(props) {
    super(props);
    this.state = {}

    this.updateState = this.updateState.bind(this);
  }

  updateState(stateTarget: any, callback?: any) {
    const { updateMainState, mainState } = this.props;
    updateMainState({ ...mainState, ...stateTarget }, callback);
  }

  render() {
    const { mainState } = this.props;
    const { gameState, selectedLetter } = mainState;
    return (
      <View>
        <TouchableHighlight onPress={() => this.updateState({ showingRewards: false })} style={{ backgroundColor: "blue", borderRadius: 5, padding: 10, position: "absolute", top: 50, right: 20 }}>
          <Text style={{ color: "#fff" }}>Game</Text>
        </TouchableHighlight>
        <View style={rewardStyles.alphabetContainer}>
          {alphabet.map(letter => (
            <Text key={letter} style={rewardStyles.alphabetLetter} onPress={() => this.updateState({ selectedLetter: letter })}>{letter}</Text>
          ))}
        </View>
        <View>
          <View style={[rewardStyles.alphabetContainer, { marginTop: 60, flexWrap: "wrap", justifyContent: "flex-start" }]}>
            {gameState.map((state, index) => (
              state.letter === selectedLetter &&
              <View key={index} style={{ marginRight: 5, marginTop: 5, borderColor: "#000", borderRadius: 5, borderWidth: 1, backgroundColor: "#fafafa", padding: 10 }}>
                <Text>letra: {state.letter}</Text>
                <Text>recompensa: {state.reward}</Text>
              </View>
            ))}
          </View>
        </View>
      </View>
    )
  }
}

export default Rewards