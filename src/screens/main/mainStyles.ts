import { StyleSheet } from "react-native";

export default StyleSheet.create({
  iconColor: {
    color: "#fff"
  }
});