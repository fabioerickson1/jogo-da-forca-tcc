// React
import React, { Component } from "react";

// Icons
import { FontAwesome } from "@expo/vector-icons";

// Styles
import styles from "./mainStyles";

// Tabs
import Game from "../game/game";
import Graphs from "../graphs/graphs";

// Components
import { Container, Text, Tabs, Tab, TabHeading } from "native-base";
import alphabet from "../../helpers/alphabet";
import Rewards from "../rewards/rewards";

interface IProps { }
interface IState {
  arrayLost?: Array<number>;
  arrayWin?: Array<number>;
  errorCount?: number;
  gamesLost?: number;
  gameState?: Array<{ letter: string, reward: number }>;
  gamesWin?: number;
  percentage?: number;
  rightShoot?: number;
  running?: boolean;
  selectedLetter?: string;
  selectedWord?: string;
  selectedWordLength?: number;
  showingGraph?: boolean;
  showingLosts?: boolean;
  showingRewards?: boolean;
  stateAlphabet?: Array<string>;
  wordArray?: Array<string>;
  wrongWordArray?: Array<string>;
}

class Main extends Component<IProps, IState> {
  constructor(props) {
    super(props);
    this.state = {
      arrayLost: [0],
      arrayWin: [0],
      errorCount: 6,
      gamesLost: 0,
      gameState: [],
      gamesWin: 0,
      percentage: 20,
      rightShoot: 0,
      running: true,
      selectedLetter: "",
      selectedWord: null,
      selectedWordLength: 0,
      showingGraph: false,
      showingLosts: false,
      showingRewards: false,
      stateAlphabet: alphabet,
      wordArray: [],
      wrongWordArray: []
    }

    this.updateMainState = this.updateMainState.bind(this);
  }

  updateMainState(stateTarget: any, callback?: any) {
    this.setState({ ...stateTarget }, () => {
      if (callback) callback(this.state);
    });
  }

  render() {
    const gameIcon = <TabHeading><Text><FontAwesome name="gamepad" size={30} /></Text></TabHeading>
    const graphIcon = <TabHeading><Text><FontAwesome name="line-chart" size={30} /></Text></TabHeading>
    const rewardIcon = <TabHeading><Text><FontAwesome name="sort-alpha-asc" size={30} /></Text></TabHeading>
    return (
      <Container>
        <Tabs tabBarPosition="bottom">
          <Tab heading={gameIcon}>
            <Game updateMainState={this.updateMainState} mainState={this.state} />
          </Tab>
          <Tab heading={graphIcon}>
            <Graphs updateMainState={this.updateMainState} mainState={this.state} />
          </Tab>
          <Tab heading={rewardIcon}>
            <Rewards updateMainState={this.updateMainState} mainState={this.state} />
          </Tab>
        </Tabs>
      </Container>
    )
  }
}

export default Main