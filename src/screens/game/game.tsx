import React, { Component } from "react";
import { View, Text, Image, TouchableHighlight, Dimensions, CheckBox, Slider } from "react-native";
import gameStyles from "./gameStyles";
import dictionary from "../../helpers/dictionary";
import forca from "../../../assets/forca.png";
import stick from "../../../assets/stick.png";
import alphabet from "../../helpers/alphabet";
import { VictoryLine, VictoryChart, VictoryTheme } from "victory-native";

import * as MediaLibrary from 'expo-media-library';
import * as FileSystem from 'expo-file-system';
import * as Permissions from 'expo-permissions';

interface IMainState {
  arrayLost?: Array<number>;
  arrayWin?: Array<number>;
  errorCount?: number;
  gamesLost?: number;
  gameState?: Array<{ letter: string, reward: number }>;
  gamesWin?: number;
  percentage?: number;
  rightShoot?: number;
  running?: boolean;
  selectedLetter?: string;
  selectedWord?: string;
  selectedWordLength?: number;
  showingGraph?: boolean;
  showingLosts?: boolean;
  showingRewards?: boolean;
  stateAlphabet?: Array<string>;
  wordArray?: Array<string>;
  wrongWordArray?: Array<string>;
}

interface IProps {
  updateMainState?: any;
  mainState?: IMainState;
}

interface IState {
}

class Game extends Component<IProps, IState> {
  constructor(props) {
    super(props);
    this.state = {
    }

    this.calcReward = this.calcReward.bind(this);
    this.endGame = this.endGame.bind(this);
    this.finalizePlay = this.finalizePlay.bind(this);
    this.findALetter = this.findALetter.bind(this);
    this.handleStopGame = this.handleStopGame.bind(this);
    this.saveFile = this.saveFile.bind(this);
    this.shootALetter = this.shootALetter.bind(this);
    this.startGame = this.startGame.bind(this);
    this.updateState = this.updateState.bind(this);
  }

  componentDidMount() {
    this.startGame();
  }

  calcReward(letter: string, type: boolean) {
    return new Promise((resolve) => {
      const { mainState } = this.props;
      const { gameState, selectedWord } = mainState;
      const gameStateMapped: Array<{ letter: string, reward: number }> = JSON.parse(JSON.stringify(gameState));
      const index = gameStateMapped.findIndex(state => state.letter === letter);
      if (index < 0) {
        gameStateMapped.push({
          letter: letter,
          reward: type === true ? 100 : -100,
        })
      } else {
        gameStateMapped[index] = {
          letter: letter,
          reward: type === true ? gameStateMapped[index].reward + 100 : gameStateMapped[index].reward - 100,
        }
      }
      this.updateState({ gameState: gameStateMapped }, () => {
        resolve();
      });
    })
  }

  endGame() {
    const { mainState } = this.props;
    const { arrayLost, arrayWin, errorCount, gamesLost, gamesWin } = mainState;
    const arrayLostMapped = JSON.parse(JSON.stringify(arrayLost));
    const arrayWinMapped = JSON.parse(JSON.stringify(arrayWin));
    if (errorCount <= 0) {
      arrayLostMapped.push(gamesLost + 1);
      arrayWinMapped.push(gamesWin);
    } else {
      arrayLostMapped.push(gamesLost);
      arrayWinMapped.push(gamesWin + 1);
    }
    this.updateState({
      stateAlphabet: alphabet,
      wordArray: [],
      wrongWordArray: [],
      errorCount: 6,
      rightShoot: 0,
      gamesLost: errorCount <= 0 ? gamesLost + 1 : gamesLost,
      gamesWin: errorCount <= 0 ? gamesWin : gamesWin + 1,
      arrayLost: arrayLostMapped,
      arrayWin: arrayWinMapped
    }, () => {
      this.startGame();
    })
  }

  finalizePlay(errorCountMapped: number, wordArrayMapped: Array<string>, wrongWordArrayMapped: Array<string>, type: boolean) {
    const { mainState } = this.props;
    const { rightShoot } = mainState;
    this.updateState({
      errorCount: errorCountMapped,
      wordArray: wordArrayMapped,
      wrongWordArray: wrongWordArrayMapped,
      rightShoot: type ? rightShoot + 1 : rightShoot
    }, () => {
      const { mainState } = this.props;
      const { errorCount, rightShoot, selectedWord, running } = mainState;
      const selectedWordArray = selectedWord.split("");
      const unique = selectedWordArray.filter((letter, i) => selectedWordArray.indexOf(letter) === i);
      if (unique.length === rightShoot || errorCount === 0) this.endGame();
      else {
        if (running) setTimeout(this.shootALetter, 0);
      }
    });
  }

  findALetter() {
    const { mainState } = this.props;
    const { gameState, percentage, stateAlphabet } = mainState;
    const stateAlphabetMapped: Array<string> = JSON.parse(JSON.stringify(stateAlphabet));
    let letter: string = "";

    const randomNumber = Math.floor(Math.random() * 100);
    if (randomNumber <= percentage) {
      const filteredGameState = gameState.filter(as => stateAlphabet.includes(as.letter));
      if (filteredGameState.length > 0) {
        const rewards = filteredGameState.map(item => item.reward);
        const maxReward = Math.max(...rewards);
        const sameRewardLetters = filteredGameState.filter(item => item.reward === maxReward);
        const iof = Math.floor(Math.random() * sameRewardLetters.length);
        letter = sameRewardLetters[iof].letter;
      } else {
        letter = stateAlphabetMapped[Math.floor(Math.random() * stateAlphabetMapped.length)];
      }
    } else {
      letter = stateAlphabetMapped[Math.floor(Math.random() * stateAlphabetMapped.length)];
    }

    stateAlphabetMapped.splice(stateAlphabetMapped.findIndex((alphabetLetter: any) => alphabetLetter === letter), 1);
    this.updateState({ stateAlphabet: stateAlphabetMapped });
    return letter;
  }

  handleStopGame() {
    const { mainState } = this.props;
    const { running } = mainState;
    this.updateState({ running: !running }, (stateTarget: IMainState) => {
      const { running } = stateTarget;
      if (running) this.startGame();
    });
  }

  saveFile = async () => {
    const { mainState } = this.props;
    const { gameState } = mainState;
    const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
    if (status === "granted") {
        let fileUri = FileSystem.documentDirectory + "text.txt";
        await FileSystem.writeAsStringAsync(fileUri, JSON.stringify(gameState), { encoding: FileSystem.EncodingType.UTF8 });
        const asset = await MediaLibrary.createAssetAsync(fileUri)
        await MediaLibrary.createAlbumAsync("Download", asset, false)
    }
}

  shootALetter() {
    const { mainState } = this.props;
    const { errorCount, selectedWord, wordArray, wrongWordArray } = mainState;
    let errorCountMapped: number = JSON.parse(JSON.stringify(errorCount));
    const wordArrayMapped: Array<string> = JSON.parse(JSON.stringify(wordArray));
    const wrongWordArrayMapped: Array<string> = JSON.parse(JSON.stringify(wrongWordArray));
    const letter: string = this.findALetter();
    if (!selectedWord.includes(letter)) {
      errorCountMapped -= 1;
      wrongWordArrayMapped.push(letter);
      this.calcReward(letter, false).then(() => {
        this.finalizePlay(errorCountMapped, wordArrayMapped, wrongWordArrayMapped, false);
      });
    } else {
      wordArrayMapped.push(letter);
      this.calcReward(letter, true).then(() => {
        this.finalizePlay(errorCountMapped, wordArrayMapped, wrongWordArrayMapped, true);
      });
    }
  }

  startGame() {
    let word = dictionary[Math.floor(Math.random() * dictionary.length)];
    while (word.length < 4 || word.length > 10) {
      word = dictionary[Math.floor(Math.random() * dictionary.length)];
    }
    this.updateState({ selectedWord: word, selectedWordLength: word.length }, () => {
      this.shootALetter();
    });
  }

  updateState(stateTarget: any, callback?: any) {
    const { updateMainState, mainState } = this.props;
    updateMainState({ ...mainState, ...stateTarget }, callback);
  }

  render() {
    const { mainState } = this.props;
    const { arrayLost, arrayWin, errorCount, gamesLost, gamesWin, percentage, selectedWordLength, selectedWord, showingGraph, gameState, running, selectedLetter, showingLosts, showingRewards, wordArray, wrongWordArray } = mainState;

    const getSpaceWidth = { width: (85 / selectedWordLength).toString().concat("%") };
    const renderWordLetter = (index: number) => wordArray.findIndex(letter => letter === selectedWord.charAt(index)) !== -1 && selectedWord.charAt(index);
    const renderIncorrectLetter = (letter: string, index: number) => letter.concat(index + 1 !== wrongWordArray.length ? " - " : "")

    const winData = arrayWin.map((actWin, i) => ({ x: i, y: actWin }));
    const lostData = arrayLost.map((actLost, i) => ({ x: i, y: actLost }));

    return (
      <View style={gameStyles.container}>
        {showingRewards ?
          <View>
            <TouchableHighlight onPress={() => this.updateState({ showingRewards: false })} style={{ backgroundColor: "blue", borderRadius: 5, padding: 10, position: "absolute", top: 50, right: 20 }}>
              <Text style={{ color: "#fff" }}>Game</Text>
            </TouchableHighlight>
            <View style={gameStyles.alphabetContainer}>
              {alphabet.map(letter => (
                <Text key={letter} style={gameStyles.alphabetLetter} onPress={() => this.updateState({ selectedLetter: letter })}>{letter}</Text>
              ))}
            </View>
            <View>
              <View style={[gameStyles.alphabetContainer, { marginTop: 60, flexWrap: "wrap", justifyContent: "flex-start" }]}>
                {gameState.map((state, index) => (
                  state.letter === selectedLetter &&
                  <View key={index} style={{ marginRight: 5, marginTop: 5, borderColor: "#000", borderRadius: 5, borderWidth: 1, backgroundColor: "#fafafa", padding: 10 }}>
                    <Text>letra: {state.letter}</Text>
                    <Text>recompensa: {state.reward}</Text>
                  </View>
                ))}
              </View>
            </View>
          </View>
          : <View>
              <View style={{ display: "flex", flexDirection: "row", justifyContent: "space-between" }}>
                <Text style={{ marginRight: 10 }}>{selectedWord}</Text>
                <Text style={{ marginRight: 10 }}>Win: {gamesWin}</Text>
                <Text style={{ marginRight: 10 }}>Lost: {gamesLost}</Text>
                <TouchableHighlight onPress={() => this.updateState({ showingGraph: true })} style={{ backgroundColor: "#ff9933", borderRadius: 5, padding: 10, position: "absolute", top: 20, left: 300 }}>
                  <Text style={{ color: "#fff" }}>Gráfico</Text>
                </TouchableHighlight>
                <TouchableHighlight onPress={() => this.updateState({ showingRewards: true })} style={{ backgroundColor: "#ff9933", borderRadius: 5, padding: 10, position: "absolute", top: 20, left: 400 }}>
                  <Text style={{ color: "#fff" }}>Recompensas</Text>
                </TouchableHighlight>
                <TouchableHighlight onPress={this.handleStopGame} style={{ backgroundColor: running ? "#ff0000" : "#0000ff", borderRadius: 5, padding: 10, position: "absolute", top: 20, left: 550 }}>
                  <Text style={{ color: "#fff" }}>{running ? "Parar" : "Iniciar"}</Text>
                </TouchableHighlight>
                <TouchableHighlight onPress={() => this.saveFile()} style={{ backgroundColor: "#0000ff", borderRadius: 5, padding: 10, position: "absolute", top: 20, left: 650 }}>
                  <Text style={{ color: "#fff" }}>Salvar</Text>
                </TouchableHighlight>
                <View style={{ position: "absolute", top: 200, left: 300 }}>
                  <Slider style={{ width: 400 }} maximumValue={100} minimumValue={0} value={percentage} onValueChange={(value) => this.updateState({ percentage: value })} />
                  <Text>{percentage.toString().concat(" %")}</Text>
                </View>
              </View>
              <View style={gameStyles.gameContainer}>
                <Image source={forca} width={180} height={380} />
                <Image source={stick} style={gameStyles.stick} />
                {errorCount >= 6 && <View style={gameStyles.head} />}
                {errorCount >= 5 && <View style={gameStyles.body} />}
                {errorCount >= 4 && <View style={gameStyles.leftArm} />}
                {errorCount >= 3 && <View style={gameStyles.rightArm} />}
                {errorCount >= 2 && <View style={gameStyles.leftLeg} />}
                {errorCount >= 1 && <View style={gameStyles.rightLeg} />}
                <View style={gameStyles.wrongLettersContainer}>
                  {wrongWordArray.map((letter, index) => (
                    <Text style={gameStyles.wrongLetters} key={index}>{renderIncorrectLetter(letter, index)}</Text>
                  ))}
                </View>
                <View style={gameStyles.wordContainer}>
                  {[...Array(selectedWordLength)].map((e, index) => (
                    <View key={index} style={[gameStyles.letterSpace, getSpaceWidth]}>
                      <Text style={gameStyles.letter}>{renderWordLetter(index)}</Text>
                    </View>
                  ))}
                </View>
              </View>
            </View>
        }
      </View>
    )
  }
}

export default Game;