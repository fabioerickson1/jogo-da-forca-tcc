import React from "react";
import Main from "./screens/main/main";
import { useKeepAwake } from 'expo-keep-awake';

function AppConfig() {
  useKeepAwake();
  return <Main />
}

export default AppConfig;
