import React from 'react';
import { AppLoading } from 'expo';
import { Container, StyleProvider } from 'native-base';
import * as Font from 'expo-font';
import { Ionicons } from '@expo/vector-icons';
import Main from './src/screens/main/main';
import { StyleSheet, StatusBar } from 'react-native';
import getTheme from './native-base-theme/components';
import material from './native-base-theme/variables/commonColor';
import { ScreenOrientation } from 'expo';

interface Iprops { }
interface Istate {
  isReady?: boolean
}

export default class App extends React.Component<Iprops, Istate> {
  constructor(props) {
    super(props);
    this.state = {
      isReady: false,
    };

    this.changeOrientation = this.changeOrientation.bind(this);
  }

  async componentDidMount() {
    this.changeOrientation();
    await Font.loadAsync({
      Roboto: require('native-base/Fonts/Roboto.ttf'),
      Roboto_medium: require('native-base/Fonts/Roboto_medium.ttf'),
      ...Ionicons.font,
    });
    this.setState({ isReady: true });
  }

  async changeOrientation() {
    await ScreenOrientation.lockAsync(ScreenOrientation.OrientationLock.LANDSCAPE);
  }

  render() {
    const { isReady } = this.state;
    if (!isReady) {
      return <AppLoading />;
    }

    return (
      <StyleProvider style={getTheme(material)}>
        <Container style={styles.container}>
          <StatusBar hidden={true} />
          <Main />
        </Container>
      </StyleProvider>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});